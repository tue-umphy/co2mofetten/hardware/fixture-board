# 🔩 Fixture Board

This is a customizable, 3D-printable board. It is useful if you want to screw something to a wall that has holes for screws, but at the wrong locations. 

## Usage

- Clone the repository: 
  - with SSH: `git clone --recursive git@gitlab.com:tue-umphy/co2mofetten/hardware/fixture-board.git`
  - or with HTTPS: `git clone --recursive https://gitlab.com/tue-umphy/co2mofetten/hardware/fixture-board.git`
- Open the `*.scad` file with [OpenSCAD](https://openscad.org)
- Modify the parameters to fit your needs. You can also use one of the parameter sets in the top selection list of the Customizer as starting point.
- Press F6 to render, then F7 to save as STL
