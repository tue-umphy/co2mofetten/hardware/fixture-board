// clang-format off
include <utils/utils.scad>;
include <Round-Anything/polyround.scad>;
// clang-format on

/* [🚪 Board] */

// clang-format off

// How the outer shape of the board should look like. Choose "🚗 auto" to just fit the outline of the holes and fixtures.
board_shape = "⬛ square"; // ["⬛ square", "⚫ circle", "📁 file","🔺 polygon","🚗 auto"]

// clang-format on

// If you set board_shape="📁 file", this is the path to that SVG shape file.
board_shape_file = "";
// If you specified board_shape="🔺 polygon", these are [X1, Y1, X2, Y2, ...] points of the board outline.
board_shape_vertices = [ -50, -50, 50, -50, 50, 50, -50, 50 ]; // [-150:0.1:150]
board_shape_vertices_ = groupn(board_shape_vertices, 2);
// The board will be resized to this [width, height]. Use [0,0] to skip resizing.
board_size = [ 50, 50 ]; // [0:0.1:300]
// Smoothing of the board outline
board_edge_radius = 1; // [0:0.1:50]
// Thickness of the board
board_thickness = 3; // [0:0.1:10]
// How the board should be filled.
board_fill_pattern = "➕ grid"; // ["⬛ solid", "➕ grid"]
// For board_fill_pattern="➕ grid", how to rotate that grid
board_fill_rotation = 0; // [-180:0.1:180]
// For board_fill_pattern="➕ grid", smoothing radius for that grid
board_fill_edge_radius = 1; // [0:0.1:50]
// For board_fill_pattern="➕ grid", thickness of the grid lines
board_fill_stroke_width = 3; // [0:0.1:30]
// For board_fill_pattern="➕ grid", size of the grid holes
board_fill_hole_size = 4; // [0:0.1:50]
// Radius of solid material around holes and fixtures. Useful for strengthening when using board_fill_pattern="➕ grid"
board_fill_support = 4; // [0:0.1:30]
// Make sure that the board_fill_support does not exceed the board outline
board_fill_support_inside = true;

/* [🔩 Fixtures] */
// Positions [X1, Y1, X2, Y2, ...] of the fixtures
fixture_positions = [ -10, -10, 0, 0, 10, 10 ]; // [-150:0.1:150]
fixture_positions_ = groupn(fixture_positions, 2);
// Offset [X, Y, Z] of the fixtures
fixture_position_offset = [ 0, 0, 0 ]; // [-150:0.1:150]
// Size of the fixtures. Interpretation depends on fixture_shape.
fixture_size = [ 5, 5 ]; // [0:0.1:50]
// Shape of the fixtures.
fixture_shape = "🔘 holepin"; // ["⬛ square", "⚫ circle", "🔘 holepin", "📁 file"]
// For fixture_shape="📁 file", this is the path to the SVG file.
fixture_shape_file = "";
// offset (horizontal enlargement/shrinkage) of the fixture shape
fixture_offset = 0; // [-50:0.1:50]
// how far the fixtures should extend from the board
fixture_height = 5; // [0:0.1:30]

/* [⚫ Holes] */
// Show the hole shapes removed from the board
hole_debug = false;
// Positions [X1, Y1, X2, Y2, ...] of the fixtures
hole_positions = [ 0, 10, 0, -10, -10, 0, 10, 0 ]; // [-150:0.1:150]
hole_positions_ = groupn(hole_positions, 2);
// Horizontal offset [X, Y] of the holes on the board
hole_position_offset = [ 0, 0 ]; // [-150:0.1:150]
// Size of the hole
hole_size = [ 5, 5 ]; // [0:0.1:50]
// Shape of the hole
hole_shape = "⚫ circle"; // ["⬛ square", "⚫ circle", "📁 file"]
// For hole_shape="📁 file", this is the path to the SVG file.
hole_shape_file = "";
// Offset (horizontal enlargement/shrinkage) of the hole shape
hole_offset = 0; // [-50:0.1:50]
// How larger the "entrance" to the hole should be
hole_countersink_offset = 1; // [0:0.1:10]
// How deep this enlarged hole shape should reach into the board
hole_countersink_depth = 1.5; // [0:0.1:10]
// How deep the transition from countersunk shape to the actual hole shape should be
hole_countersink_transition = 0.5; // [0:0.1:10]

// determine max [width,height] of polygon points
function get_max_width_height(points) = [
  max([for (p = points) p.x]) - min([for (p = points) p.x]),
  max([for (p = points) p.y]) - min([for (p = points) p.y])
];

// determine center of polygon points
function get_center(points) = [
  mean([for (p = points) p.x]),
  mean([for (p = points) p.y]),
];

board_size_ = min(board_size) > 0
                ? board_size
                : (board_shape == "🔺 polygon"
                     ? get_max_width_height(board_shape_vertices_)
                     : (board_shape == "🚗 auto"
                          ? get_max_width_height(concat(hole_positions_,
                                                        fixture_positions_))
                          : board_size));

board_center =
  board_shape == "🔺 polygon"
    ? get_center(board_shape_vertices_)
    : (board_shape == "🚗 auto"
         ? get_center(
             concat([for (p = hole_positions_) p + hole_position_offset],
                    [for (p = fixture_positions_) p + fixture_position_offset]))
         : [ 0, 0 ]);

module resize_unless_zero(s)
{
  if (min(s) > 0)
    resize(s) children();
  else
    children();
}

module
board_base_shape()
{
  offset(board_edge_radius) offset(-board_edge_radius)
    resize_unless_zero(board_size)
  {
    if (board_shape == "⬛ square")
      square(board_size, center = true);
    else if (board_shape == "⚫ circle")
      circle(max(board_size));
    else if (board_shape == "📁 file")
      import(board_shape_file);
    else if (board_shape == "🔺 polygon")
      polygon(board_shape_vertices_);
    else if (board_shape == "🚗 auto")
      offset(board_fill_support + board_fill_stroke_width) hull()
      {
        translate(hole_position_offset) at(hole_positions_)
          circle(d = max(hole_size) / 2);
        translate(fixture_position_offset) at(fixture_positions_)
          circle(d = max(fixture_size) / 2);
      }
  }
}

module
board_shape()
{
  module fill_support()
  {
    offset(board_fill_support)
    {
      translate(hole_position_offset) at(hole_positions_) hole_shape();
      translate(fixture_position_offset) at(fixture_positions_) fixture_shape();
    }
  }
  if (board_fill_pattern == "⬛ solid") {
    board_base_shape();
  } else {
    shell2d(-board_fill_stroke_width)
    {
      board_base_shape();
      translate(board_center) rotate([ 0, 0, board_fill_rotation ]) difference()
      {
        square(2 * norm(board_size_), center = true);
        // Increase this N if the pattern doesn't fill everything
        n = ceil(max(board_size_) /
                 (board_fill_hole_size + board_fill_stroke_width));
        offset(board_fill_edge_radius) offset(-board_fill_edge_radius)
          mosaic(ix = [-n:n],
                 iy = [-n:n],
                 distance = board_fill_hole_size + board_fill_stroke_width)
            square(board_fill_hole_size, center = true);
      }
      if (board_fill_support_inside)
        fill_support();
    }
    if (!board_fill_support_inside)
      fill_support();
  }
}

module
fixture_shape()
{
  offset(fixture_offset) if (fixture_shape == "⬛ square")
  {
    square(fixture_size, center = true);
  }
  else if (fixture_shape == "⚫ circle")
  {
    resize(fixture_size) circle(max(fixture_size));
  }
  else if (fixture_shape == "🔘 holepin") { circle(d = max(fixture_size)); }
  else if (fixture_shape == "📁 file")
  {
    resize(fixture_shape) import(fixture_shape_file);
  }
}

module
hole_shape()
{
  offset(hole_offset) if (hole_shape == "⬛ square")
  {
    square(hole_size, center = true);
  }
  else if (hole_shape == "⚫ circle")
  {
    resize(hole_size) circle(max(hole_size));
  }
  else if (hole_shape == "📁 file")
  {
    resize(hole_shape) import(hole_shape_file);
  }
}

// Precision
epsilon = 0.1 * 1;
$fa = $preview ? 2 : 0.5;
$fs = $preview ? 1 : 0.5;

difference()
{
  union()
  {
    // the board
    render_if(board_fill_pattern != "⬛ solid", convexity = 2)
      linear_extrude(board_thickness) board_shape();

    // fixtures
    translate(fixture_position_offset) translate([ 0, 0, board_thickness ])
      at(fixture_positions_) linear_extrude(fixture_height) fixture_shape();
  }
  // holes
  highlight_if(hole_debug) translate(hole_position_offset) at(hole_positions_)
    chained_hull()
  {
    // hole
    translate([ 0, 0, -epsilon ]) linear_extrude(epsilon) hole_shape();
    translate([
      0,
      0,
      board_thickness - hole_countersink_depth -
      hole_countersink_transition
    ]) linear_extrude(epsilon) hole_shape();
    translate([
      0,
      0,
      board_thickness -
      hole_countersink_depth

    ]) linear_extrude(epsilon) offset(hole_countersink_offset) hole_shape();
    translate([ 0, 0, board_thickness ]) linear_extrude(epsilon)
      offset(hole_countersink_offset) hole_shape();
  }
  // fixture holes
  if (fixture_shape == "🔘 holepin")
    translate([ 0, 0, -epsilon / 2 ])
      translate([for (i = [0:1]) fixture_position_offset[i]])
        at(fixture_positions_) hull()
    {
      d = fixture_size[0] == fixture_size[1] ? max(fixture_size) / 2
                                             : min(fixture_size);
      at(fixture_position_offset[2] >= 0
         ? [ 0, fixture_position_offset[2] + fixture_height + board_thickness ]
         : [ min(fixture_position_offset[2] + board_thickness, fixture_position_offset[2]), max(fixture_height+board_thickness,board_thickness) ],
       "z") cylinder(d = d, h = epsilon);
    }
}
